import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class TestCases {
    WebDriver driver;
    WebDriverWait wait;
    final String baseUrl = "http://localhost:5555/wd/hub";
    DesiredCapabilities caps;

    @BeforeMethod
    public void testSetup() throws MalformedURLException {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        caps = new DesiredCapabilities();
        caps.setBrowserName("chrome");
        driver = new RemoteWebDriver(new URL(baseUrl), caps);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        wait = new WebDriverWait(driver, 10);

    }

    @Test
    public void verifyUserCanLoginWithValidCredentials() {
        driver.get("http://166.62.92.171/feedback360");
        WebElement email_textfield = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@name,'email')]")));
        //email_textfield.sendKeys("ny206822");
        //driver.findElement(By.xpath("//*[contains(@name,'password')]")).sendKeys("x");
        driver.findElement(By.cssSelector(".md-primary.md-button")).click();
        WebElement profile_pic = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".md-toolbar-tools-bottom")));
        Assert.assertTrue(profile_pic.isDisplayed(), "Login did not successful");
    }


    @Test
    public void verifyUserCanNotLoginWithInvalidCredentials() throws InterruptedException {
        driver.get("http://166.62.92.171/feedback360");
        WebElement email_textfield = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@name,'email')]")));
        WebElement password_textfield = driver.findElement(By.xpath("//*[contains(@name,'password')]"));
        WebElement login_button = driver.findElement(By.cssSelector(".md-primary.md-button"));

        email_textfield.clear();
        email_textfield.sendKeys("@#$%SF");

        password_textfield.clear();
        password_textfield.sendKeys("abcd");

        login_button.click();
        Thread.sleep(5000);
        Assert.assertTrue(isAlertPresent(), "Alert is not present!!");
    }


    @Test
    public void verifyManagerCanSeeTheTasks() throws InterruptedException {
        final String expected_data_for_first_row = "Due date March 3rd, 2018";
        driver.get("http://166.62.92.171/feedback360");
        WebElement email_textfield = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(@name,'email')]")));
        driver.findElement(By.cssSelector(".md-primary.md-button")).click();
        WebElement profile_pic = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".md-toolbar-tools-bottom")));
        Assert.assertTrue(profile_pic.isDisplayed(), "Login did not successful");

        WebElement tasks_button = driver.findElements(By.cssSelector(".inset+.inset")).get(1);
        tasks_button.click();

        Thread.sleep(2000);
        WebElement first_row_data = driver.findElement(By.cssSelector("tr>td:nth-child(2)"));
        String actual_data_for_first_row = first_row_data.getText();

        Assert.assertEquals(actual_data_for_first_row, expected_data_for_first_row);
    }


    @AfterMethod
    public void afterMethod(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE || result.getStatus() == ITestResult.SKIP) {
            this.takeScreenshot(driver, "failure-screenshots/" + result.getMethod().getMethodName().toString() + ".jpg");
        }
        driver.close();
    }

    public void takeScreenshot(WebDriver driver, String filePath) throws IOException {
        TakesScreenshot screenshot = (TakesScreenshot) driver;
        File file = screenshot.getScreenshotAs(OutputType.FILE);
        File destFile = new File(filePath);
        FileUtils.copyFile(file, destFile);
    }

    public boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

}
